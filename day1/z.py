import sys

def fifi(a,b):
    return a+b

dispatch = {
    'fifi': fifi
}

def main(function_name, args):
    if function_name not in dispatch:
        print 'no command ' + function_name
        return

    dispatch[function_name](*args)

if __name__ == '__main__':
    main(sys.argv[1], sys.argv[2:])