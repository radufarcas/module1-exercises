# Exemplu Conway's game of life
# =============================
# Game of life e un automat celular.
# Anumite reguli se executa pentru fiecare pozitie dintr-un grid
# O pozitie poate fi vie sau moarta.
# . . . .
# . . o .
# . . . o
# . o o o
# O celula se naste traieste si moare in functie de cati din vecini sunt vii
# v v v   <-- vecinii
# v . v
# v v v
# Any live cell with fewer than two live neighbours dies
# Any live cell with two or three live neighbours lives
# Any live cell with more than three live neighbours dies
# Any dead cell with exactly three live neighbours becomes a live cell


# Alegem structura de date cea mai simpla: o lista de linii, unde o linie e o lista de bool
# la 1, 2 avem o celula vie grid[1][2] == True

#TODO: first implement the tests!

def neighbour_population(grid, i, j):
    count = 0
    for di in [-1, 0, 1]:
        for dj in [-1, 0, 1]:
            if not di ==  dj == 0 and (0 <= i + di <= 16 and 0 <= j + dj <= 16):
                count += 1 if grid[(j+dj) % 16][(i + di) % 16] else 0
    return count
    # pass


def kernel(grid, i, j):
    pass


def step(grid, next_grid):
    # boundary conditions fixed to dead cells
    pass


def parse_grid(s):
    grid = []
    for sline in s.splitlines():
        if sline.strip():
            grid.append(sline)
    return grid
    #pass

def test_grid(grid):
    s = """
    . . o
    . o .
    o . .
    """
    test = [
        [False, False, True]
        [False, True, False]
        [True, False, False]
    ]
    assert parse_grid(s) == test



def print_grid(grid):
    pass


test_grid = parse_grid("""
. . . . . . . . . . . . . . . .
. . . . . o . . . . . . . . . .
. . . . . . o . . . . . . . . .
. . . . o o o . . . . . . . . .
. . . . . . . . . . . . . . . .
. . . . . . . . . . . . . . . .
. . . . . . . o . . . . . . . .
. . o . . . . . . . . . . . . . 
. . o o . . . . o . . . . . . .
. . o . . . . o . . . . . . . .
. . . . . . . . o . . . . . . .
. . . . . . . . o . . . . . . .
. . . o . o . . . . . . . . . .
. . . . o . o o . . . . . . . .
. . . o . o o . . . . . . . . .
. . . . . . . . . . . . . . . .
""")

def test_parse_grid():
    pass

def test_step():
    pass


def usage():
    print 'commands:'
    print '  q - exit'
    print '  n - step'
    print '  t 2 4 - toggle cell 2, 4'

def console_tui(initial_grid):
    grid = initial_grid
    grid_next = [[False] * len(l) for l in grid]
    print_grid(grid)
    usage()

    while True:
        com = raw_input('>').strip().split()
        if not com:
            usage()
        elif com[0] == 'q':
            return
        elif com[0] == 'n':
            print_grid(grid)
            step(grid, grid_next)
            grid, grid_next = grid_next, grid
            print
        elif com[0] == 't':
            try:
                i, j = int(com[1]), int(com[2])
                grid[i][j] = not grid[i][j]
                print_grid(grid)
            except (ValueError, IndexError):
                usage()
        else:
            usage()
        print

if __name__ == '__main__':
    console_tui(test_grid)
