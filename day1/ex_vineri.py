def ranger(start, end=None, step=1):
    if end is None:
        end = start
        start = 0
    ret = []
    e = start

    while True:
        if step > 0:
            if e >= end:
                break
        elif step < 0:
            if e <= end:
                break
        else:
            raise ValueError('0 step')

        ret.append(e)
        e += step

    return ret

print ranger(5, 1, -1)