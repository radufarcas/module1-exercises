# insertion sort is a simple widely used sort
# --------------

# wikipedia gives this pseudocode:

# for i = 1 to length(A)
# x = A[i]
# j = i - 1
# while j >= 0 and A[j] > x
#     A[j + 1] <- A[j]
#     j <- j - 1
# end while
#     A[j + 1] = x
# end for

# implement the insertion sort in python

def insort(lst):
    # todo
    return lst
    pass


# Given two lists sorted in increasing order, create and return a merged
# list of all the elements in sorted order.
# The solution should make a single  pass of both lists.
def merge(a, b):
    # todo
    result = []
    i = j = 0
    while i < len(a) and j < len(b):
        if a[i] <= b[i]:
            result.append(a[i])
            i += 1
        else:
            result.append(b[i])
            j += 1
    while i < len(a):
        result.append(a[i])
        i += 1
    while j < len(b):
        result.append(b[i])
        j += 1

    return result

# merge sort is the simplest to understand efficient sort
# ----------
# It splits the list in the middle, and sorts each half recursively.
# Then it merges the two sorted lists using the above merge function.
# If a list has less than 2 elements it is already sorted and no longer split.

# A list of length 2**8 can be split maximum 8 times.
# The cost of merge is proportional with list len.
# So total cost is n_splits * cost_of_merge = n log(n)
# Implement the merge sort

def merge_sort(lst):
    pass

#----- testing code below ---

def test_merge():
    assert merge(['aa', 'xx', 'zz'], ['bb', 'cc']) == ['aa', 'bb', 'cc', 'xx', 'zz']
    assert merge(['aa', 'xx'], ['bb', 'cc', 'zz']) == ['aa', 'bb', 'cc', 'xx', 'zz']
    assert merge(['aa', 'aa'], ['aa', 'bb', 'bb']) == ['aa', 'aa', 'aa', 'bb', 'bb']

def test_merge_sort():
    assert merge_sort([]) == []
    assert merge_sort([7, 2, 3, 1]) == [1, 2, 3, 7]
    assert merge_sort([1, 2]) == [1, 2]
    assert merge_sort([3, 2]) == [2, 3]
    assert merge_sort(['z', 'a', 'b', 'q']) == ['a', 'b', 'q', 'z']
    assert merge_sort(list('merge')) == list('eegmr')

def test_insort():
    assert insort([]) == []
    assert insort([7, 2, 3, 1]) == [1, 2, 3, 7]
    assert insort([1, 2]) == [1, 2]
    assert insort([3, 2]) == [2, 3]
    assert insort(['z', 'a', 'b', 'q']) == ['a', 'b', 'q', 'z']
    assert insort(list('merge')) == list('eegmr')


if __name__ == '__main__':
    # test_insort()
    test_merge()
    test_merge_sort()

