def unflatten(seq):
    root = {}
    for path in seq:
        current_node = root
        for el in path.split('/'):
            if el not in  current_node:
                current_node[el] = {}
            current_node = current_node[el]

    return root


def test_unflatten():
    input = ['A/B/T', 'A/U', 'A/U/Z']
    expected = {
        'A': {
            'B': {'T': {}},
            'U': {
                'Z': {}
            }
        }
    }
    assert unflatten(input) == expected