def chunked(seq, chunk_size):
    it = iter(seq)
    while True:
        c = []
        try:
            for e in xrange(chunk_size):
                c.append(next(it))
        except StopIteration:
            return
        finally:
            yield c

new_gen = chunked('anaaremere', 3)

for e in new_gen:
    print e