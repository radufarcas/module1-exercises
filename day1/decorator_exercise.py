# def count(func):
#     counter = [0]
#
#     def wrapper(*args):
#         counter[0] += 1
#         print counter[0]
#         return func(*args)
#     return wrapper
#
# @count
# def test():
#     print 'radu'
#

import json

def jsonizer(func):
    def wrapped(*args):

        new_args = [json.loads(a) for a in args]
        ret = func(*new_args)
        return json.dumps(ret)
    return wrapped

@jsonizer
def test1(a, b):
    return a

print test1('{"a": 53 }', '4')