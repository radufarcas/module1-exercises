# coding=utf-8
import codecs
import urllib2

f = urllib2.urlopen(u'https://ko.wikipedia.org/wiki/나무'.encode('utf-8'))
text = f.read()

with open('file.txt', 'w') as my_file:
    i = 0
    for line in text.splitlines():
        my_file.write((unicode(i) + u"  " + line.decode('utf-8')).encode('utf-16'))
        my_file.write(u'\n'.encode('utf-16'))
        i += 1

f_res = codecs.open('file.txt', 'r', encoding='utf-16')

print f_res.readline()
print f_res.readline()
print f_res.readline()