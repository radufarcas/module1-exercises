# ***Variabile, identitate
# a. something = 3 // variabilele nu au tip in python
# b. a = [1], b = a, a.append(2)
# c.
# d. imutabile
# e. a = 3, b = a
# f. l = [1, 3, 4], l2 = l[:]
# g. d = { 1: 'a', 2: 'b', 3: 'c'}, d2 = d
# h. l1 = [1, 2, [3, 4], [2, 1], 2], l2 = l1

# ***Scalari
# a. nu exista
# b. a == b
# c. True

# ***Iteratie
# a.
# b. cheile
# c. iteritems
# d. {k:v for k, v in 'ana'}
# e.