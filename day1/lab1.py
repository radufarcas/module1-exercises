# Exercises for notions learned in unit 1
# ---------------------------------------

# takes a message, capitalizes the first letter then underlines the message with -
# print make_title('ana')
# Ana
# ---
# use the string method upper
def make_title(message):
    return message.capitalize() + '\n' + '-' * len(message)
    # pass

# given a list of tuples of length 2 it returns a list of the tuples inverted
# if both values in the tuple are the same then we omit them from the output
# [(2, 4), (3, 3), (1, 2)] will become [(4, 2), (2, 1)]
def invert_tuples(lst):
    l = []
    for v, k in lst:
        if k != v:
            l.append((k,v))
    # pass

# curses is a dictionary of cursewords to censored curses
# Given a list of words replace the curses according to the dict
# censor(['you', 'fucker'], {'fucker': 'f**er'}) should return
# ['you', 'f**er']
def censor(lst, curses):
    return [curses[e] if e in curses.keys() else e for e in lst]
    # pass

# Given a string s, return a string made of the first 2
# and the last 2 chars of the original string,
# so 'spring' yields 'spng'. However, if the string length
# is less than 2, return instead the empty string.
def both_ends(s):
    if len(s) < 2:
        return ''
    else:
        return s[0:2] + s[-2:]
    # pass

# return the factorial of n
# factorial(5) = 1*2*3*4*5
def factorial(n):
    if n == 1 or n == 0:
        return 1
    for i in range(1, n):
        n *= i
    return n
    # pass

# return a list of fibonacci numbers smaller than n
def fibo(n):
    lst = []
    a = 0
    lst.append(a)
    b = 1
    for i in range(n):
        a, b = b, a + b
        if a < n:
            lst.append(a)
    print lst
    return lst
    # pass

#----- testing code below ---

def test_make_title():
    assert make_title('ana') == 'Ana\n---'

def test_invert_tuples():
    assert invert_tuples([(2, 4), (3, 3), (1, 2)]) == [(4, 2), (2, 1)]

def test_censor():
    assert censor(['you', 'fucker'], {'fucker': 'f**er'}) == ['you', 'f**er']

def test_both_ends():
    assert both_ends('spring') == 'spng'
    assert both_ends('Hello') == 'Helo'
    assert both_ends('a') == ''
    assert both_ends('xyz') == 'xyyz'

def test_fibo():
    assert fibo(10) == [0, 1, 1, 2, 3, 5, 8]

def test_factorial():
    assert factorial(5) == 1*2*3*4*5
    assert factorial(1) == 1
    assert factorial(0) == 1

