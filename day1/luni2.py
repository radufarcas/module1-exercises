# coding=utf-8


from urllib2 import urlopen

import codecs

f = codecs.open('./file.txt', 'a', encoding='utf-8')

encoded = codecs.encode(u'http://ko.wikipedia.org/wiki/나무', 'utf-8')
url = urlopen(encoded)

for i, line in enumerate(url):
        f.write(u"%d : %s" % (i, codecs.decode(line, 'utf-8')))

f.readline()
f.readline()
f.readline()

f.close()