def zeros(nrows, ncols):
    return [[0] * ncols for _ in xrange(nrows)]

print zeros(3, 3)

def print_m(m):
    for r in m:
        print ' '.join(str(a) for a in r)

print print_m(zeros(3,3))

def tr(m):
    res = zeros(len(m[0]), len(m))
    for i in xrange(len(m)):
        for j in xrange(len(m[0])):
            res[j][i] = m[i][j]
    return res

print tr([[1,2],[3,4],[6,8]])

#   tema pe maine
# [1 2 2 3 4 4 5 2] => [1 2 3 4 5 2]

text = "povesti cu mere si cu pere si cu mere"

def create_dict(s):
    d = {}
    for word in s.split():
        if word in d:
            d[word] += 1
    else:
        d[word] = 1
    return d

print create_dict(text)

def swap(s):
    newDict = {}
    for k, v in d.iteritems():
        if v in newDict:
            newDict[v].append(k)
        else:
            newDict[v] = [k]
    return newDict

print swap(text)