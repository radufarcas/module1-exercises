def numbered_lines(lines):
    for i, line in enumerate(lines):
        yield str(i) + ':' + line

def paginate(lines, page_size, page_separator='\n\n'):
    for i, line in enumerate(lines):
        yield line
        if i % page_size == 0:
            yield page_separator


gen = numbered_lines(['ana', 'are'])


print gen
