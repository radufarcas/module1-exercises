from contextlib import contextmanager
import  datetime

@contextmanager
def myformat(path, mode):
    f = None
    try:
        f = open(path, mode)
        if mode == 'w':
            f.write('WOOFA!\n')
        elif mode == 'r':
            first_line = next(f)
            if first_line != 'WOOFA!\n':
                raise ValueError('bad woofa file')
        yield f
    finally:
        if f is not None:
            f.close()

with myformat('lala.woofa', 'r') as f:
    for l in f:
        print l
